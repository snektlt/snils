package org.snils_check.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.dynalink.beans.StaticClass;
import org.apache.commons.lang.RandomStringUtils;
import org.snils_check.modelsJson.Requestion;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.tinylog.Logger;

import javax.sound.sampled.Port;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.UUID;


@Controller
public class JSONController {

    @GetMapping(value = "snils")
    public Object getResponse (@RequestParam(value = "snils") String id) throws IOException {
        long startTime = System.currentTimeMillis();
        String templateResponse = Files.readString(Paths.get("src\\main\\resources\\files\\templates\\json\\getResponse.json"), StandardCharsets.UTF_8);
        System.out.println("templateResponse:");
        System.out.println(templateResponse);
        String randomBalance = RandomStringUtils.randomNumeric(4);
        String randomAddress = RandomStringUtils.randomAlphanumeric(50);
        UUID uuid = UUID.randomUUID();

        LocalDateTime dt = LocalDateTime.now();
        DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        String formattedDT = dtFormatter.format(dt);
        String responseBody = String.format(templateResponse, id, uuid, randomBalance, randomAddress, formattedDT);
        Logger.info(String.format("Заглушка отработала за %s мс. ID клиента - %s. UUID ответа - %s.", System.currentTimeMillis() - startTime,id,uuid));
        return ResponseEntity.ok().header("content-type","application/json").body(responseBody);


    }

    @PostMapping(value = "snils")
    public Object postResponse (@RequestBody String requestBody) throws IOException {
        long startTime = System.currentTimeMillis();
        String templateResponse = Files.readString(Paths.get("src\\main\\resources\\files\\templates\\json\\postResponse.json"), StandardCharsets.UTF_8);
        UUID uuid = UUID.randomUUID();
        LocalDateTime dt = LocalDateTime.now();
        DateTimeFormatter dtFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
        String lastActiveDt = dtFormatter.format(dt);

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Requestion Requestion = objectMapper.readValue(requestBody, Requestion.class);
            String snils = Requestion.getSnils();
            String message = Requestion.getMessage();
            String responseBody = String.format(templateResponse, snils,message, uuid);

            snils = snils.replace("-","");
            String snilsCH = snils.substring(10,12);
            snils = snils.substring(0,9);
            int CH = Integer.parseInt(snils.substring(0,1))*9 + Integer.parseInt(snils.substring(1,2))*8 + Integer.parseInt(snils.substring(2,3))*7 + Integer.parseInt(snils.substring(3,4))*6 + Integer.parseInt(snils.substring(4,5))*5 + Integer.parseInt(snils.substring(5,6))*4 + Integer.parseInt(snils.substring(6,7))*3 + Integer.parseInt(snils.substring(7,8))*2 + Integer.parseInt(snils.substring(8,9))*1;
            CH = CH % 101;

            if(Integer.parseInt(snilsCH) != CH){
                Logger.error(String.format("%s\n",requestBody));
                return ResponseEntity.badRequest().header("content-type","application/json").body(String.format("{\"message\": \"Некорректный снилс!\", \"request\": \"%s\"}",requestBody));
            } else {
                Logger.info(String.format("Заглушка отработала за %s мс. Snils клиента - %s. message - %s, UUID ответа - %s.", System.currentTimeMillis() - startTime, snils, message, uuid));
                return ResponseEntity.ok().header("content-type", "application/json").body(responseBody);
            }

        } catch (Exception e) {
            Logger.error(String.format("%s\n%s",e.getMessage(),requestBody));
            return ResponseEntity.badRequest().header("content-type","application/json").body(String.format("{\"message\": \"Передана невалидная json\", \"request\": \"%s\"}",requestBody));
        }
    }
}
